package uz.pdp.lesson10task1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson10task1Application {

    /*Mehmonxona strukturasini CRUD asosida yozing, bunda Hotel(name),
     Room(number, floor, size, Hotel)
    class lar bo'lsin. Hotel id orqali shu mehmonxonaga
    tegishli room lar ro'yxatini pageable qilib olib keluvchi method yozing.
    Proyektni git ga public qilib yuklang va vazifaga javob sifatida shu
    git repository ning link address ni yuboring.*/

    public static void main(String[] args) {
        SpringApplication.run(Lesson10task1Application.class, args);
    }

}
