package uz.pdp.lesson10task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson10task1.entity.Hotel;
import uz.pdp.lesson10task1.service.HotelServise;

import java.util.List;

@RestController
@RequestMapping("/hotel")
public class HotelController {
    @Autowired
    HotelServise hotelServise;

    @GetMapping
    public List<Hotel> getAllHotel(){
        return hotelServise.getAllHotel();
    }

    @GetMapping("/{id}")
    public Hotel getByIdHotel(@PathVariable Integer id){
        return hotelServise.getByIdHotel(id);
    }

    @DeleteMapping("/{id}")
    public String deleteHotel(@PathVariable Integer id){
        return hotelServise.deleteByIdHotel(id);
    }

    @PostMapping
    public String addHotel(@RequestBody Hotel hotel){
        return hotelServise.addHotel(hotel);
    }


    @PutMapping("/{id}")
    public String editHotel(@PathVariable Integer id, @RequestBody Hotel hotel){
        return hotelServise.editHotel(id, hotel);
    }

}
