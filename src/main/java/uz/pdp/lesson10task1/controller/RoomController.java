package uz.pdp.lesson10task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import uz.pdp.lesson10task1.entity.Room;
import uz.pdp.lesson10task1.payload.RoomDto;
import uz.pdp.lesson10task1.service.RoomService;

import java.awt.print.Pageable;
import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {
    @Autowired
    RoomService roomService;

    @GetMapping("/roomsByHotelId/{hotelId}")
    public List<Room> getAllRoom(@PathVariable Integer hotelId, @RequestParam int page){
        return (List<Room>) roomService.getRoomsByHotelId(hotelId, page );
    }

    @GetMapping("/{id}")
    public Room getByIdRoom(@PathVariable Integer id){
       return roomService.getByIdRoom(id);
    }

    @DeleteMapping("/{id}")
    public String deleteRoom(@PathVariable Integer id){
        return roomService.deleteRoom(id);
    }

    @PostMapping
    public String addRoom(@RequestBody RoomDto roomDto){
        return roomService.addRoom(roomDto);
    }

    @PutMapping("/{id}")
    public String editRoom (@PathVariable Integer id, @RequestBody RoomDto roomDto){
        return roomService.editRoom(id, roomDto);
    }




}
