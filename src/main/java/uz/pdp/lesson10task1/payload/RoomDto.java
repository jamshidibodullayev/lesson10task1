package uz.pdp.lesson10task1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.lesson10task1.entity.Hotel;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomDto {

    private Integer id;

    private int number;

    private int floor;

    private double size;

    private Integer hotelId;
}
