package uz.pdp.lesson10task1.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson10task1.entity.Hotel;

public interface HotelRepository extends JpaRepository<Hotel, Integer> {

}
