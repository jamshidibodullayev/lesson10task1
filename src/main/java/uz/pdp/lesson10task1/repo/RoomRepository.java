package uz.pdp.lesson10task1.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.lesson10task1.entity.Room;

import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Integer> {

    boolean existsByNumberAndHotel(Integer room, Integer hotel);

    Page<Room> findAllByHotelId(Integer hotelId, Pageable pageable);


}
