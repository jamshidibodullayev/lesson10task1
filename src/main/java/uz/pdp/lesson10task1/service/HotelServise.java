package uz.pdp.lesson10task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.lesson10task1.entity.Hotel;
import uz.pdp.lesson10task1.repo.HotelRepository;

import java.util.List;
import java.util.Optional;

@Service
public class HotelServise {
    @Autowired
    HotelRepository hotelRepository;

    public List<Hotel> getAllHotel() {
        return hotelRepository.findAll();
    }


    public Hotel getByIdHotel(Integer id) {
        Optional<Hotel> byIdHotel = hotelRepository.findById(id);
        return byIdHotel.orElse(null);
    }


    public String deleteByIdHotel(Integer id) {
        Optional<Hotel> byId = hotelRepository.findById(id);
        if (byId.isPresent()){
            try {
                hotelRepository.deleteById(byId.get().getId());
                return "O`chirildi";
            }catch (Exception e){
                return "Bu Hotelda Roomlasr bog`langan";
            }
        }return "Siz kiritgan Id lik hotel mavjud emas";
    }


    public String addHotel(Hotel hotel) {
        if (hotel.getName().length()>0){
            try {
                Hotel save = hotelRepository.save(hotel);
                return "Saqlandi. Id: "+save.getId();
            }catch (Exception e){
                return "Siz kiritgan nomli mehmonxona bor";
            }
        }return "Siz Hotel nomini kiritmagansiz";
    }


    public String editHotel(Integer id, Hotel hotel) {
    if (hotel.getName().length()>0){
        Optional<Hotel> byIdHotel = hotelRepository.findById(id);
        if (byIdHotel.isPresent()){
            byIdHotel.get().setName(hotel.getName());
            try {
                hotelRepository.save(byIdHotel.get());
                return "O`zgartirildi";
            }catch (Exception e){
                return "Siz kiritgan nomli Hotel mavjud";
            }
        }
    }return "Siz Hotel nomini kiritmagansiz";



    }
}
