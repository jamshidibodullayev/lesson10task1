package uz.pdp.lesson10task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.lesson10task1.entity.Hotel;
import uz.pdp.lesson10task1.entity.Room;
import uz.pdp.lesson10task1.payload.RoomDto;
import uz.pdp.lesson10task1.repo.HotelRepository;
import uz.pdp.lesson10task1.repo.RoomRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

@Service
public class RoomService {
    @Autowired
    RoomRepository roomRepository;

    @Autowired
    HotelRepository hotelRepository;


    public Page<Room> getRoomsByHotelId(Integer hotelId, int page) {
        Pageable pageable = (Pageable) PageRequest.of(page, 5);
        Page<Room> roomPage = roomRepository.findAllByHotelId(hotelId, pageable);
        return roomPage;
    }


    public Room getByIdRoom(Integer id) {
        Optional<Room> byIdRoom = roomRepository.findById(id);
        return byIdRoom.orElse(null);
    }


    public String deleteRoom(Integer id) {
        Optional<Room> byIdHotelRoom = roomRepository.findById(id);
        if (byIdHotelRoom.isPresent()){
            roomRepository.deleteById(id);
            return "O`chirildi";
        }return "Siz kiritgan Id lik Room mavjud emas";
    }


    public String addRoom(RoomDto roomDto) {
        Optional<Hotel> optionalHotel = hotelRepository.findById(roomDto.getHotelId());
        if (optionalHotel.isPresent()){
            Room room=new Room();
            room.setNumber(roomDto.getNumber());
            room.setFloor(roomDto.getFloor());
            room.setSize(roomDto.getSize());
            room.setHotel(optionalHotel.get());
            try {
                Room saveRoom = roomRepository.save(room);
                return "Siz kiritgan xona siz tanlagan mehmonxonaga qo`shildi";

            }catch (Exception e){
                return "Siz kiritgan xonalik mehmonxona avvaldan mavjud";
            }

        }
        return "Siz tanlagan Id lik Hotel mavjud emas.";

    }

    public String editRoom(Integer id, RoomDto roomDto) {
        Optional<Room> optionalRoom = roomRepository.findById(id);
        if (optionalRoom.isPresent()){
            Optional<Hotel> optionalHotel = hotelRepository.findById(roomDto.getHotelId());
           if(optionalHotel.isPresent()){
               Room room=optionalRoom.get();
               room.setHotel(optionalHotel.get());
               room.setFloor(roomDto.getFloor());
               room.setNumber(roomDto.getNumber());
               room.setNumber(roomDto.getNumber());
               try {
                   roomRepository.save(room);
                   return "Saqlandi";

               }catch (Exception e){
                   return "Siz kiritgan xonalik mehmonxona mavjud";
               }


           }
           return "Siz kiritgan hotel mavjud emas.";



        }return "Siz kiritgan id lik room mavjud emas.";
    }
}
